// ignore_for_file: avoid_unnecessary_containers

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_dqhao/models/post.dart';
import 'package:flutter_dqhao/services/remote_service.dart';

class Bai21 extends StatefulWidget {
  const Bai21({Key? key}) : super(key: key);

  @override
  State<Bai21> createState() => _Bai21State();
}

class _Bai21State extends State<Bai21> {
  List<Post>? posts;
  var isLoaded = false;
  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    posts = await RemoteService().getPosts();
    if (posts != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isLoaded,
      // ignore: sort_child_properties_last
      child: ListView.builder(
        itemCount: posts?.length,
        itemBuilder: (context, index) => Container(
          padding: EdgeInsets.all(16),
          child: Row(children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Colors.grey[300]),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                
                  Text(
                    posts![index].title.toString(),
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    posts![index].body.toString(),
                    style: TextStyle(fontSize: 15),
                  ),
                ],
              ),
            ),
          ]),
          //child: Text(posts![index].title),
        ),
      ),
      replacement: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
