import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai7 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.red, // background
              onPrimary: Colors.white, // foreground
            ),
            onPressed: () {},
            child: const Text('Buttons'),
          ),
          const Text(
              "ĐỜI NGƯỜI ĐÂU MẤY LẦN VUI Dù đục dù trong, con sông vẫn chảy Dù cao dù thấp, cây lá vẫn xanh Dù người phàm tục hay kẻ tu hành Vẫn phải sống từ những điều rất nhỏ Ta hay chê rằng cuộc đời méo mó Sao ta không tròn ngay tự trong tâm Đất ấp ôm cho mọi hạt nảy mầm"),
        ],
      ),
    );
  }
}
