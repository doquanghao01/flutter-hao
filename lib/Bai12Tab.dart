// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai12 extends StatefulWidget {
  const Bai12({Key? key}) : super(key: key);

  @override
  State<Bai12> createState() => _Bai12State();
}

class _Bai12State extends State<Bai12> with TickerProviderStateMixin {
  late TabController _tabController;
  @override
  initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          controller: _tabController,
          // ignore: prefer_const_literals_to_create_immutables
          tabs: [
            Tab(
              icon: Icon(Icons.cloud_outlined),
            ),
            Tab(
              icon: Icon(Icons.beach_access_sharp),
            ),
            Tab(
              icon: Icon(Icons.brightness_5_sharp),
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          Center(
            child: Text("Thời tiết"),
          ),
          Center(
            child: Text("Chiếc ô"),
          ),
          Center(
            child: Text("Cài đặt"),
          )
        ],
      ),
    );
  }
}
