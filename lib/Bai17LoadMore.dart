// ignore_for_file: prefer_const_constructors, avoid_print, prefer_final_fields

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai17 extends StatefulWidget {
  const Bai17({Key? key}) : super(key: key);

  @override
  State<Bai17> createState() => _Bai17State();
}

class _Bai17State extends State<Bai17> {
  List dummyList = [];
  ScrollController _scrollController = ScrollController();
  int _currentMax = 15;
  @override
  void initState() {
    super.initState();
    dummyList = List.generate(_currentMax, (index) => "Item :${index + 1}");
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreList();
      }
    });
  }

  void _getMoreList() {
    print("Go more list");
    for (int i = _currentMax; i < _currentMax + 10; i++) {
      dummyList.add("Item : ${i + 1}");
    }
    _currentMax = _currentMax + 10;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: _scrollController,
      itemExtent: 100,
      itemBuilder: (BuildContext context, int index) {
        if (index == dummyList.length) {
          return CupertinoActivityIndicator();
        }
        return ListTile(
          title: Text(dummyList[index]),
        );
      },
      itemCount: dummyList.length + 1,
    );
  }
}
