// ignore_for_file: prefer_const_constructors, prefer_final_fields

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai15 extends StatefulWidget {
  const Bai15({Key? key}) : super(key: key);

  @override
  State<Bai15> createState() => _Bai15State();
}

class _Bai15State extends State<Bai15> {
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          SizedBox(height: 20),
          TextField(
            controller: _email,
            obscureText: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
            ),
          ),
          SizedBox(height: 20),
          TextField(
            controller: _password,
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Passwrod',
            ),
          ),
          SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              if (_email.text.endsWith('Admin') &&
                  _password.text.endsWith('admin')) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      const AlertDialog(title: Text('Dang nhap thanh cong')),
                );
              }
            },
            child: Text('Login'),
          ),
        ],
      ),
    );
  }
}
