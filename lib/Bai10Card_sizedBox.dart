// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai10 extends StatelessWidget {
  const Bai10({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      // ignore: prefer_const_literals_to_create_immutables
      children: [
        Center(
          child: Card(
            color: Colors.blue,
            child: SizedBox(
              width: 300,
              height: 100,
              child: Center(
                  child: Text(
                'Elevated Card',
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
        ),
        Center(
          child: Card(
            elevation: 0,
            color: Colors.red,
            child: const SizedBox(
              width: 300,
              height: 100,
              child: Center(
                  child: Text(
                'Filled Card',
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
        ),
        Center(
          child: Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: Colors.green,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(12)),
            ),
            child: const SizedBox(
              width: 300,
              height: 100,
              child: Center(child: Text('Outlined Card')),
            ),
          ),
        ),
      ],
    );
  }
}
