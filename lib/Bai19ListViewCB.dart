// ignore_for_file: prefer_const_constructors, implementation_imports, unnecessary_import, prefer_final_fields

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';

class Bai19 extends StatefulWidget {
  const Bai19({Key? key}) : super(key: key);

  @override
  State<Bai19> createState() => _Bai19State();
}

class _Bai19State extends State<Bai19> {
  Color _color = Color.fromARGB(255, 255, 153, 0);
  final europeanCountries = [
    'Albania',
    'Andorra',
    'Armenia',
    'Austria',
    'Azerbaijan',
    'Belarus',
    'Belgium',
    'Bosnia and Herzegovina',
    'Bulgaria',
    'Croatia',
    'Cyprus',
    'Czech Republic',
    'Denmark',
    'Estonia',
    'Finland',
    'France',
    'Georgia',
    'Germany',
    'Greece',
    'Hungary',
    'Iceland',
    'Ireland',
    'Italy',
    'Kazakhstan',
    'Kosovo',
    'Latvia',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macedonia',
    'Malta',
    'Moldova',
    'Monaco',
    'Montenegro',
    'Netherlands',
    'Norway',
    'Poland',
    'Portugal',
    'Romania',
    'Russia',
    'San Marino',
    'Serbia',
    'Slovakia',
    'Slovenia',
    'Spain',
    'Sweden',
    'Switzerland',
    'Turkey',
    'Ukraine',
    'United Kingdom',
    'Vatican City'
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: europeanCountries.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
          width: double.maxFinite,
          height: 160,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: _color,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.network(
                'https://gamek.mediacdn.vn/133514250583805952/2021/1/10/ouuu3-1610271707141720119484.jpg',
                height: 160,
              ),
              Expanded(
                child: Text(
                  europeanCountries[index],
                  style: GoogleFonts.rubikMoonrocks(
                      textStyle: TextStyle(color: Colors.white, fontSize: 30)),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: (() {
                    setState(() {
                      _color == Colors.green
                          ? _color = Color.fromARGB(255, 255, 153, 0)
                          : _color = Colors.green;
                    });
                  }),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
