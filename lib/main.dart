// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter_dqhao/Bai12Tab.dart';
import 'package:flutter_dqhao/Bai14drawer.dart';
import 'package:flutter_dqhao/Bai15TextField.dart';
import 'package:flutter_dqhao/Bai21API.dart';
import 'package:flutter_dqhao/Bai7Buttons_Text.dart';
import 'package:flutter_dqhao/Bai9Stack.dart';
import 'Bai10Card_sizedBox.dart';
import 'Bai11SnackBar.dart';
import 'Bai16LoadImgURL.dart';
import 'Bai17LoadMore.dart';
import 'Bai18listview.dart';
import 'Bai19ListViewCB.dart';
import 'Bai3HelloWrod.dart';
import 'Bai4ListView.dart';
import 'Bai5Navigator.dart';
import 'Bai6.dart';
import 'Bai8GridView.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Flutter cơ bản"),
          ),
          body: const Bai21(),
        ));
  }
}
