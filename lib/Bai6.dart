import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Bai6 extends StatelessWidget {
  const Bai6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Image.asset('images.jpg'),
        ),
        Row(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Icon(Icons.sixty_fps),
            const Text("Sinh viên thiện!"),
          ],
        ),
      ],
    );
  }
}
